### Deutsch

# Generelles

- [Die Migrationsanleitung](https://gitlab.opencode.de/iqsh/collaboration-online-board/index/-/blob/main/MIGRATIONGUIDE.md)

# Versionsverlauf

## Version 2.0.0

### Funktionen:
- Frontend: 
  - Austausch des Frontend-Frameworks zu TailwindCSS. Partielles Redesign:
    - Login-Seiten
    - Nutzenden-Überblick
    - Post-Optik auf Pinnwänden
    - Administrations-Menü auf der Pinnwand
    - Touch-Fähigkeit verbessert
  - KI: Chat-Bot zur Generierung von Text- und Bild-Inhalten mit OpenAI kompatiblen Schnittstellen
    - in configuration.json die Variablen des Bereichs ai und aiServer eintragen
    - in de.json folgenden Texte hinzufügen:
      - ai komplett übernehmen
      - dashboardPage.ai.label
      - general.errors.warning
  - Weitere Tooltips eingeführt:
    - in de.json folgende Texte hinzufügen:
      - tooltips.connectPost
      - tooltips.settings
      - tooltips.expandPost
      - tooltips.like
      - tooltips.reject
      - tooltips.expandColumn
      - tooltips.advancedSettings
      - tooltips.grid
      - tooltips.list
      - contentPage.post.notification
      - contentPage.post.notification.authorize
      - contentPage.post.notification.authorize.label
      - dashboardPage.folder.label
      - general.errors.notSupported
      - general.variables.copy
      - general.variables.owner
      - general.variables.owner.label
  - Versions-Fehlermeldung überarbeitet:
    - in de.json folgende Texte hinzufügen:
      - general.errors.unsupported.* (den gesamten Abschnitt inkl. der Unterpunkte)
- AI:
  - Neuer Server für die Generierung von Text- und Bild-Inhalten mit OpenAI kompatiblen Schnittstellen
  
  
### Fehlerbehebungen:
- Frontend: 
  - Fehlende Labels eingepflegt (in de.json folgende Texte hinzufügen):
    - contentPage.markLabel
    - contentPage.media.label -> „Medien und Kommentar(e)“ auf „Kommentar(e)“
    - contentPage.media.delete -> Medieneinbindung -> Kommentar
    - contentPage.textEditor.colorReset
    - general.variables.loadImage.label

### Development:
* Farben müssen noch in style.css definiert werden (wenn man die Farben von TailwindCSS verwenden will, also dann bspw. .blue-700{@apply bg-blue-700} oder man definiert auch eigene Farben)
* Weitere Schritte zur Anpassung des Frontends für die eignen Ansprüche in der README-Datei vom Frontend nachlesen


## Version 1.3.0

### Funktionen:

- Ordnerstruktur:
  - in de.json von Frontend folgende Texte hinzufügen:
    - dashboardPage.folder.create.label
    - dashboardPage.folder.create.submit
    - dashboardPage.folder.create.name
    - dashboardPage.folder.create.main
    - dashboardPage.folder.create.color
    - dashboardPage.folder.edit.label
    - dashboardPage.folder.edit.submit
    - dashboardPage.folder.add
    - dashboardPage.folder.delete.label
    - dashboardPage.folder.delete.sureDelete
    - dashboardPage.folder.delete.deletePanes
    - dashboardPage.folder.delete.warnPanes
    - dashboardPage.folder.addMessage
    - dashboardPage.folder.move.message
    - dashboardPage.folder.move.label
    - dashboardPage.folder.move.submit
    - dashboardPage.filterBy.date.ascendingIcon
    - dashboardPage.filterBy.date.descendingIcon
  - in configution.json von Frontend die Variable folderColors eintragen
- Favoriten:
  - in de.json von Frontend folgende Texte hinzufügen:
    - dashboardPage.favorites.label
    - dashboardPage.favorites.add.label
    - dashboardPage.favorites.add.message
    - dashboardPage.favorites.delete.label
    - dashboardPage.favorites.delete.message
    - dashboardPage.types.my
- Autorisierung: Neue Posts können autorisiert werden (optionales Flag)
  - in de.json von Frontend folgende Texte hinzufügen:
    - adjustPanel.general.approval.short
    - adjustPanel.general.approval.long
- Minimap auf Freie Form: Eine kleine Karte, auf der die Elemente angezeigt werden und durch Anklicken wird die Ansicht auf den Punkt auf der Minimap verschoben
- Dashboard-Informationsbanner:
  - in de.json von Frontend folgende Texte hinzufügen:
    - indexPage.updateText.dashboardUpdate
- Wenn für die Moderatorenseite kein Passwort gesetzt ist, wird beim Zugriffsversuch eine Fehlermeldung angezeigt:
  - in de.json von Frontend folgende Texte hinzufügen:
    - general.errors.noPasswordSet
    - general.errors.noModPassword
- Coordinator-Server-Statistiken Endpoint (erreichbar unter COORDINATOR-IP:PORT/stats via HTTP-GET)
  - in .env von Coordinator folgende Variable hinzufügen:
    - COORDINATOR_STATISTICS_PORT=PORT
- Color-Picker: Nutzende können nun mit einem Color-Picker eigene Farben auswählen
- Auf der Tabellen-Form können Elemente nun auch links angefügt werden

### Development:

- Alle Backend-Datenbank-Migrationen laufen ab dieser Version über Knex.js ab, so müssen Änderungen an der Datenbank nicht mehr händisch durchgeführt werden. Um die Migration durchzuführen folgenden Befehl ausführen: `knex migrate:latest`. Anweisungen dafür gibt es in der Readme.

## Version 1.2.0

### Fehlerbehebungen:

- Kommentar-Seitenleiste: Visuelle Fehlerbehebung beim Upload von Dateien

### Funktionen:

- Neue Oberflächen-Art: Zeitstrahl
  - Änderungen dafür notwendig
    - migration004_1670398599021.sql einspielen
    - in de.json von Frontend folgende Texte hinzufügen:
      - general.paneTypes.timeline
      - adjustPanel.general.elementwidth
      - dashboardPage.types.timeline
    - in configuration.json von Frontend wurde das Array "timelineLineColors" hinzugefügt
- Freie Form: Optimiertes Handling beim Verschieben von Elementen
- Tabellenform: Beim Kopieren wird das Element unterhalb des Originals eingefügt

### Development:

- Datei-Upload: Dateiendungen / MimeTypes können dynamisch definiert werden
  - Backend: in .env folgende Variablen hinzufügen mit den gewünschten Werten (Komma getrennt)(kann aus der .env.example kopiert werden)
    - BACKEND_FILE_UPLOAD_EXTENSIONS
    - BACKEND_FILE_UPLOAD_MIMETYPES
  - Frontend: in configution.json die Variable "uploadFileAccept" hinzufügen (Komma getrennter String) (kann aus der configuration.json.example kopiert werden)
- UIkit aktualisiert auf 3.16.9
- Backend: Voraussetzung für Node von 16 auf 18+ geändert

## Version 1.1.0

### Fehlerbehebungen:

- Groß- und Kleinschreibung beim Passwort-Reset: E-Mail wird nun immer kleingeschrieben.
- Statistik-Dashboard: Die Heapcharts aktualisieren sich nicht mehr bei Tageswechsel (wie beabsichtigt)
- Spalten werden wieder hinten angefügt
- Abspeicherung von Positionsdaten (Freeform-Objekte) -> kein null als String mehr
- Linie von Freier Form wird unterbrochen, wenn Objekte zu weit auseinander sind, behebt auch den Fehler, dass sich die Ansicht nach dem Bearbeiten eines Elements verschiebt
- Stream: Anpassungsmenü öffnet sich wieder
- Einbettungen: Falscher Verweis der Bilder / Linktext behoben
- Stream: Spalte lässt sich nicht mehr löschen
- Freeform: Margin links entstanden durch Titel / Untertitel
- Bibliothek für Audio-Aufnahmen ausgetauscht
- Dateibrowser beschränkt die erlaubten Dateiendungen nicht mehr, alleinige Überprüfung durch eine Abgleich-Funktion im Front- sowie Backend, da unter iOS einige (erlaubte) Dateien sonst nicht auswählbar waren

### Funktionen:

- iWork (Pages, Numbers, Keynote) Dateiendungen im Upload zugelassen
- .wscdoc-Dateien zum Hochladen zugelassen
- Einem Post können nachträglich Bilder hinzugefügt werden
- Tabellenform: Plus-Button-Position kann ausgewählt werden (oben oder unten)
  - Änderungen dafür notwendig
    - migration001_1666157332374.sql einspielen
    - in de.json von Frontend folgende Texte hinzufügen:
      - adjustPanel.general.newElementPosition.bottom
      - adjustPanel.general.newElementPosition.top
      - adjustPanel.general.newElementPosition.label
- Neue Schriftarten
  - Änderungen dafür notwendig
    - migration002_1666592393567.sql einspielen
    - in de.json von Frontend folgende Texte hinzufügen:
      - adjustPanel.general.fontFamilyInfo
      - adjustPanel.general.fontFamily
- Element / Spaltensichtbarkeit
  - Änderungen dafür notwendig
    - migration003_1666606276771.sql einspielen
    - in de.json von Frontend folgende Texte hinzufügen
      - contentPage.hideLabel
      - tooltips.hide
      - tooltips.unhide
- URLs können für eigenen Text gesetzt werden
  - Änderungen dafür notwendig
    - in de.json von Frontend folgende Texte hinzufügen
      - contentPage.textEditor.link
      - contentPage.textEditor.invalidLink

### Development:

- UIkit akualisiert auf 3.15.10

---

### English

# General

- [The Migration Guide](https://gitlab.opencode.de/iqsh/collaboration-online-board/index/-/blob/main/MIGRATIONGUIDE.md)

# Changelog

## Version 2.0.0

### Functions:
- Frontend: 
  - Replacement of the frontend framework to TailwindCSS. Partial redesign:
    - Login pages
    - User overview
    - Post optics on panes
    - Administration menu on the panes
    - Touch capability improved
  - AI: Chat bot for generating text and image content with OpenAI-compatible interfaces
    - Add the variables of the ai and aiServer area in configuration.json
    - Add the following texts to de.json:
      - Take over ai completely
      - dashboardPage.ai.label
      - general.errors.warning
  - Introduce further tooltips:
    - Add the following texts in en.json:
      - tooltips.connectPost
      - tooltips.settings
      - tooltips.expandPost
      - tooltips.like
      - tooltips.reject
      - tooltips.expandColumn
      - tooltips.advancedSettings
      - tooltips.grid
      - tooltips.list
      - contentPage.post.notification
      - contentPage.post.notification.authorize
      - contentPage.post.notification.authorize.label
      - dashboardPage.folder.label
      - general.errors.notSupported
      - general.variables.copy
      - general.variables.owner
      - general.variables.owner.label
  - Version error message revised:
    - Add the following texts in en.json:
      - general.errors.unsupported.* (the entire section including the subsections)
- AI:
  - New server for generating text and image content with OpenAI compatible interfaces
  
  
### Bug fixes:
- Frontend: 
  - Added missing labels (add following texts in de.json):
    - contentPage.markLabel
    - contentPage.media.label -> “Media and comment(s)” to “Comment(s)”
    - contentPage.media.delete -> media integration -> comment
    - contentPage.textEditor.colorReset
    - general.variables.loadImage.label

### Development:
* Colors must still be defined in style.css (if you want to use the colors of TailwindCSS, e.g. .blue-700{@apply bg-blue-700} or you can also define your own colors)
* Further steps for customizing the frontend for your own requirements can be found in the README file of the frontend


## Version 1.3.0

### Features:

- Folder structure:
  - Add the following texts in de.json of Frontend:
    - dashboardPage.folder.create.label
    - dashboardPage.folder.create.submit
    - dashboardPage.folder.create.name
    - dashboardPage.folder.create.main
    - dashboardPage.folder.create.color
    - dashboardPage.folder.edit.label
    - dashboardPage.folder.edit.submit
    - dashboardPage.folder.add
    - dashboardPage.folder.delete.label
    - dashboardPage.folder.delete.sureDelete
    - dashboardPage.folder.delete.deletePanes
    - dashboardPage.folder.delete.warnPanes
    - dashboardPage.folder.addMessage
    - dashboardPage.folder.move.message
    - dashboardPage.folder.move.label
    - dashboardPage.folder.move.submit
    - dashboardPage.filterBy.date.ascendingIcon
    - dashboardPage.filterBy.date.descendingIcon
  - enter the variable folderColors in configution.json of Frontend
- Favorites:
  - Add the following texts in de.json of Frontend:
    - dashboardPage.favorites.label
    - dashboardPage.favorites.add.label
    - dashboardPage.favorites.add.message
    - dashboardPage.favorites.delete.label
    - dashboardPage.favorites.delete.message
    - dashboardPage.types.my
- Authorization: New posts can be authorized (optional flag)
  - Add the following texts in de.json of Frontend:
    - adjustPanel.general.approval.short
    - adjustPanel.general.approval.long
- Minimap on free form: A small map on which the elements are displayed and clicking on them moves the view to the point on the minimap
- Dashboard information banner:
  - In de.json of frontend add the following texts:
    - indexPage.updateText.dashboardUpdate
- If no password is set for the moderator page, an error message is displayed when access is attempted:
  - add the following texts in en.json of Frontend:
    - general.errors.noPasswordSet
    - general.errors.noModPassword
- Coordinator server statistics endpoint (accessible under COORDINATOR-IP:PORT/stats via HTTP-GET)
  - add the following variable in .env of Coordinator:
    - COORDINATOR_STATISTICS_PORT=PORT
- Color picker: Users can now select their own colors with a color picker
- Elements can now also be added to the left of the table form

### Development:

- All backend database migrations run via Knex.js from this version onwards, so changes to the database no longer have to be made manually. To perform the migration, execute the following command: `knex migrate:latest`. Instructions can be found in the readme.
## Version 1.2.0

### Bug fixes:

- Comment sidebar: Visual bug fix when uploading files

### Features:

- New interface type: Timeline
  - Changes needed for this
    - import migration004_1670398599021.sql
    - add the following texts in de.json of frontend:
      - general.paneTypes.timeline
      - adjustPanel.general.elementwidth
      - dashboardPage.types.timeline
    - in configuration.json of frontend added array "timelineLineColors".
- Freeform: Optimized handling when moving elements
- Schedule: when copying, the element is pasted below the original one

### Development:

- File upload: File extensions / MimeTypes can be defined dynamically.
  - Backend: in .env add the following variables with the desired values (comma separated)(can be copied from .env.example)
    - BACKEND_FILE_UPLOAD_EXTENSIONS
    - BACKEND_FILE_UPLOAD_MIMETYPES
  - frontend: in configution.json add variable "uploadFileAccept" (comma separated string)(can be copied from configuration.json.example)
- UIkit updated to 3.16.9
- Backend: Prerequisite for Node changed from 16 to 18+.

## Version 1.1.0

### Bug fixes:

- Password reset case sensitive: email is now always lowercase.
- Statistics dashboard: heapcharts no longer update on day change (as intended).
- Columns are added at the end again
- Saving of position data (Freeform objects) -> no null as string anymore
- Line of Freeform is broken if objects are too far apart, also fixes bug that view moves after editing an element
- Stream: adjustment menu opens again
- Embeds: Fixed wrong reference of images / link text.
- Stream: column can no longer be deleted
- Freeform: Margin left arose by title / subtitle
- Library for audio recordings replaced
- File browser no longer restricts allowed file extensions, sole check by matching function in front- as well as backend, as under iOS some (allowed) files were otherwise not selectable

### Features:

- iWork (Pages, Numbers, Keynote) file extensions allowed in upload.
- .wscdoc files allowed to be uploaded
- Images can be added to a post afterwards
- Table shape: plus button position can be selected (top or bottom)
  - Changes necessary for this
    - import migration001_1666157332374.sql
    - add following texts in de.json of frontend:
      - adjustPanel.general.newElementPosition.bottom
      - adjustPanel.general.newElementPosition.top
      - adjustPanel.general.newElementPosition.label
- New fonts
  - Changes needed for this
    - import migration002_1666592393567.sql
    - add following texts in de.json of frontend:
      - adjustPanel.general.fontFamilyInfo
      - adjustPanel.general.fontFamily
- Element / Column visibility
  - Changes necessary for this
    - import migration003_1666606276771.sql
    - add the following texts in de.json of frontend
      - contentPage.hideLabel
      - tooltips.hide
      - tooltips.unhide
- URLs can be set for own text
  - changes necessary for this
    - add following texts in de.json of frontend
      - contentPage.textEditor.link
      - contentPage.textEditor.invalidLink

### Development:

- UIkit updated to 3.15.10
