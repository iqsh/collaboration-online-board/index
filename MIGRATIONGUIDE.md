### Deutsch
# Migrationsanleitung:  
Wenn eine neue Version des Collaboration Online Boards erstellt wird, werden die Änderungen im Changelog aufgenommen. In einigen Fällen müssen Sie Anpassungen an bestimmten Dateien und der Datenbank vornehmen. Diese Änderungen werden auch gezielt im Changelog erwähnt.   
Sofern Migrationen für die Datenbank vorliegen, sollten Sie während des Migrationsvorgangs alle dazugehörigen Server abseits von PostgreSQL offline nehmen und dann die neuen Migrationen der Reihe nach (Namen der Dateien beachten) einspielen. Um die Migrationen durchzuführen folgenden Befehl ausführen: `knex migrate:latest`.  
Sofern Änderungen an Konfigurations- oder Sprachdateien durchgeführt werden müssen, werden diese gezielt genannt und Sie können bei Bedarf die Standardwerte aus den .example-Dateien übernehmen.
    
***
### English
# Migration Guide:  
When a new version of the Collaboration Online Board is created, the changes are included in the changelog. In some cases, you may need to make adjustments to specific files and the database. These changes are also specifically mentioned in the changelog.   
If there are migrations for the database, you should take all related servers offline during the migration process, aside from PostgreSQL, and then apply the new migrations in order (note the names of the files). To perform the migration, execute the following command: `knex migrate:latest`.  
If changes need to be made to configuration or language files, these will be specifically named and you can use the default values from the .example files if required.
