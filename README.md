### Deutsch
## Collaborative Online Board
Eine kollaborative Software für Schulen;  
Nutzenden können *Oberflächen* erstellen, welche sie dann mittels eines Weblinks teilen und auf welchen sie mit anderen Nutzenden kollaborativ arbeiten können. Es existieren verschiedene Oberflächen-Arten, sodass Nutzende ihre Beiträge beispielsweise in Form einer Tabelle (Schedule / Stream), eines Zeitstrahls (Timeline) oder frei (Freeform) auf der Oberfläche anordnen können.

Beispiele:  
<img src="/images/example_1.png" width="40%" height="40%"/> <img src="/images/example_2.png" width="40%" height="40%"/>

### Struktur der Software
Das Collaborative Online Board besteht aus vier Komponenten:
* [Backend](https://gitlab.opencode.de/iqsh/collaboration-online-board/backend): Dient als RESTful-API und als Datei-Server für die von den Nutzenden hochgeladenen Dateien.
* [Coordinator](https://gitlab.opencode.de/iqsh/collaboration-online-board/coordinator): Ein Websocket-Server, welche die Daten zwischen den *Oberfläche* koordiniert.
* [Export](https://gitlab.opencode.de/iqsh/collaboration-online-board/export): Ein Headless-Browser, welcher Screenshots oder PDFs von der *Oberfläche* erstellt.
* [Frontend](https://gitlab.opencode.de/iqsh/collaboration-online-board/frontend): Verteilt die statischen Dateien (HTML, JS, CSS) zu den Nutzern.
* [AI](https://gitlab.opencode.de/iqsh/collaboration-online-board/ai): Dient als RESTful-API für die Generierung von KI-Inhalten durch die OpenAI kompatible Schnittstelle.

  

Wenn Sie über Updates des Repositorys per E-Mail informiert werden wollen, schreiben Sie uns die dafür vorgesehene E-Mail gerne an opsh@bildungsdienste.landsh.de.  
    
***
### English
## Collaborative Online Board
A collaborative software for schools;  
Users can create *panes* that they can share via a weblink on which they can collaborate with other users. There are different pane-types so that users can arrange their posts e.g. in form of a table (schedule / stream), a timeline (timeline) or freely (freeform) on the pane.

Examples:  
<img src="/images/example_1.png" width="40%" height="40%"/> <img src="/images/example_2.png" width="40%" height="40%"/>

### Structure of the software
Collaborative Online Board consists of four components:  
* [Backend](https://gitlab.opencode.de/iqsh/collaboration-online-board/backend): Serves as a RESTful-API and file server for the uploaded files of the user
* [Coordinator](https://gitlab.opencode.de/iqsh/collaboration-online-board/coordinator): Websocket-Server coordinates the data of the *panes* between the users
* [Export](https://gitlab.opencode.de/iqsh/collaboration-online-board/export): Headless-Browser, which creates screenshots and PDFs from the *pane* 
* [Frontend](https://gitlab.opencode.de/iqsh/collaboration-online-board/frontend): Serves the static HTML, JS, CSS files to the users
* [AI](https://gitlab.opencode.de/iqsh/collaboration-online-board/ai): Serves as a RESTful API for the generation of AI content through the OpenAI compatible interface.
  
  
If you would like to be notified of updates to the repository via email, please feel free to write us the designated email at opsh@bildungsdienste.landsh.de.
